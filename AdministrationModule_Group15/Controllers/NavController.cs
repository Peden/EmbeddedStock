﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdministrationModule.Domain.Entities;
using AdministrationModule.Domain.Repositories;

namespace AdministrationModule_Group15.Controllers
{
    public class NavController : Controller
    {
        private IBaseRepository<Category> _categoryRepository;

        public NavController(IBaseRepository<Category> categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }


        // GET: Nav
        public PartialViewResult Nav()
        {

            return PartialView("Partials/Nav",_categoryRepository.GetAll());
        }

        //GET: AddCategory
        [HttpGet]
        public PartialViewResult AddCategory()
        {
            return PartialView("Partials/AddCategory");
        }
        
        //POST: AddCategory
        [HttpPost]
        public ActionResult AddCategory(Category category)
        {
            if (!string.IsNullOrEmpty(category.CategoryName))
            {
             _categoryRepository.Save(new Category{CategoryName = category.CategoryName });
                return RedirectToAction("Index", "Home");
            }
            return PartialView("Partials/AddCategory");
        }


    }
}