﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdministrationModule.Domain.Entities;
using AdministrationModule.Domain.Repositories;
using AdministrationModule.Domain.Repositories.Component;
using AdministrationModule_Group15.Models;

namespace AdministrationModule_Group15.Controllers
{
    public class ComponentController : Controller
    {
        private readonly AbstractComponentRepo<Component> _componentRepository;
        private readonly IBaseRepository<Category> _categoryRepository;
        public ComponentController(AbstractComponentRepo<Component> componentRepository, IBaseRepository<Category> categoryRepository)
        {
            this._componentRepository = componentRepository;
            this._categoryRepository = categoryRepository;
        }


       //Get
        [HttpGet]
        public ViewResult Edit(int id)
        {
            Component componentToEdit = _componentRepository.FindById(id);

            return View(componentToEdit);
        }

        //Post
        [HttpPost]
        public ActionResult Edit(Component editedComponent)
        {
            if (ModelState.IsValid)
            {
                _componentRepository.Save(editedComponent);
                return RedirectToAction("Index", "Home");

            }
            else
            {
                return View(editedComponent);
            }

        }


        public ActionResult Delete(int id, int numberOfNumbersAndComments)
        {

            if (numberOfNumbersAndComments > 1)
            {
                _componentRepository.DeleteComponentAndComment(id);
                return RedirectToAction("Index", "Home");
            }

            _componentRepository.Delete(id);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Create()
        {
            // Put the list of categories on the ViewBag!
            PutAllCategoriesOnViewBag();

            return View(new ComponentViewModel());
        }

        [HttpPost]
        public ActionResult Create(ComponentViewModel vm, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                Component componentToSave = new Component(); // the component to save in db
                ComponentNumberAndComment componentNumberAndComment = new ComponentNumberAndComment(); // the object specifying series# and component# for the component

                if (file != null)
                {
                    componentToSave.ImageMimeType = file.ContentType;
                    componentToSave.Image = new byte[file.ContentLength];
                    file.InputStream.Read(componentToSave.Image, 0, file.ContentLength);
                }

                //Set data in the component that should be saved
                componentToSave.ComponentInfo = vm.ComponentInfo;
                componentToSave.DataSheetUrl = vm.DataSheetUrl;
                componentToSave.ComponentName = vm.ComponentName;
                componentToSave.ManufactureLink = vm.ManufactureLink;

                //Create the componentnumberandcomment object for this component
                componentNumberAndComment.SeriesNr = vm.SeriesNr;
                componentNumberAndComment.ComponentNumberId = vm.ComponentNumberId;
                componentToSave.ComponentNumbers.Add(componentNumberAndComment);
                

                //Find the right category for the component
                var category = _categoryRepository.FindById(vm.CategoryId);
                if (category.Components == null)
                    category.Components = new List<Component>();

                //Add the component to this category!
                category.Components.Add(componentToSave);
                _categoryRepository.Save(category);


                return RedirectToAction("Index", "Home");
            }

            // Before going back, remember to put the categories in ViewBag!
            PutAllCategoriesOnViewBag();

            return View(vm);
        }


        [HttpGet]
        public PartialViewResult AddOneMoreComponent(int id)
        {
            return PartialView("Partials/AddOneMore", new ComponentNumberViewModel {Componentid = id});
        }

        [HttpPost]
        public ActionResult AddOneMoreComponent(ComponentNumberViewModel vm)
        {
            if (ModelState.IsValid)
            {

                _componentRepository.SaveComponentNumberAndComment(new ComponentNumberAndComment {SeriesNr = vm.SeriesNr, ComponentNumberId = vm.ComponentNumber}, vm.Componentid );
                return RedirectToAction("Index", "Home");
            }

            return PartialView("Partials/AddOneMore");
        }


        [HttpGet]
        public ViewResult ComponentDetails(int id)
        {
            return View(_componentRepository.FindById(id));
        }


        [HttpGet]
        public PartialViewResult ComponentNumberDetails(int id, string seriesNumber)
        {
            ComponentNumberAndComment componentNumberAndComment = _componentRepository.FindById(id).ComponentNumbers.First(cn => cn.SeriesNr == seriesNumber);
            return PartialView("Partials/ComponentDetailModal", componentNumberAndComment);
        }

        [HttpPost]
        public ActionResult ComponentNumberDetails(ComponentNumberAndComment model)
        {
            Component component = _componentRepository.FindById(model.ComponentId);

            if (!String.IsNullOrEmpty(model.UserComment) || !String.IsNullOrEmpty(model.AdminComment))
            {
                if (component != null)
                {
                    _componentRepository.SaveComponentNumberAndComment(model, component.ComponentId);
                }
            }
            return View("ComponentDetails", component);
        }




        #region Helpers

        private void PutAllCategoriesOnViewBag()
        {

            // First get all the existing categories
            IEnumerable<Category> categoriesList = _categoryRepository.GetAll();

            // Now add these to the Viewbag as a list of SelectListItems
            ViewBag.categories = categoriesList.Select(x =>
            new SelectListItem()
            {
                Text = x.CategoryName,
                Value = x.CategoryId.ToString()
            });
        }
        #endregion



    }
}