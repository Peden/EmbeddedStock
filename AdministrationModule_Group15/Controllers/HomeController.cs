﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdministrationModule.Domain.Entities;
using AdministrationModule.Domain.Repositories;
using AdministrationModule.Domain.Repositories.Component;
using AdministrationModule_Group15.Models;

namespace AdministrationModule_Group15.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {

        private AbstractComponentRepo<Component> _componentRepository;
        private IBaseRepository<Category> _categoryRepository;

        public HomeController(AbstractComponentRepo<Component> _componentRepository, IBaseRepository<Category> _categoryBaseRepository)
        {
            this._componentRepository = _componentRepository;
            this._categoryRepository = _categoryBaseRepository;
        }

        public ActionResult Index()
        {
            CategoriesAndComponents categoryComponent = new CategoriesAndComponents
            {
                Components = _componentRepository.GetAll().ToList(),
                Categories = _categoryRepository.GetAll().ToList()
            };

            return View(categoryComponent);
        }

        //Actionmethod for returning the home view with components only for a specific category
        public ViewResult ShowComponentsInCategory(int id )
        {

            CategoriesAndComponents categoryComponent = new CategoriesAndComponents();
            categoryComponent.Components = _componentRepository.GetAllFromCategory(id).ToList();
            categoryComponent.Categories = _categoryRepository.GetAll().ToList();

            ViewBag.Title = categoryComponent.Components[0].Category.CategoryName;

          
            return View("Index", categoryComponent);
        }


    }
}