using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using AdministrationModule.Domain;
using AdministrationModule.Domain.Entities;
using AdministrationModule.Domain.Repositories;
using AdministrationModule.Domain.Repositories.Component;
using AdministrationModule.Domain.Repositories.User;
using AdministrationModule_Group15.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using Unity.Mvc4;

namespace AdministrationModule_Group15
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
 
            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            // Setup with help from :
            // http://tech.trailmax.info/2014/09/aspnet-identity-and-ioc-container-registration/
            // http://stackoverflow.com/questions/35461001/the-current-types-iuserstoreapplicationuser-and-dbconnection-are-respectivel
            // http://stackoverflow.com/questions/14649545/how-to-inject-constructor-in-controllers-by-unity-on-mvc-4

            ControllerBuilder.Current.SetControllerFactory(new UnityControllerFactory(container));

            container.RegisterType<AbstractUserRepo<ApplicationUser>, UserRepository>();
            container.RegisterType<IBaseRepository<LoanInformation>, LoanInformationRepository>();
            container.RegisterType<AbstractComponentRepo<Component>, ComponentRepository>();
            container.RegisterType<IBaseRepository<Category>, CategoryRepository>();
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>();

            container.RegisterType<IAuthenticationManager>(
    new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));

            container.RegisterType<DbContext, EmbeddedStockDbCtx>(new HierarchicalLifetimeManager());
            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(new HierarchicalLifetimeManager());
        }

    }
}