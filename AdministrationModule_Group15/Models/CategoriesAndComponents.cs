﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdministrationModule.Domain.Entities;

namespace AdministrationModule_Group15.Models
{
    public class CategoriesAndComponents
    {
        public List<Component> Components;
        public List<Category> Categories;
    }
}