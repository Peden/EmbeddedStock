﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdministrationModule_Group15.Models
{
    public class ComponentNumberViewModel
    {
        [Required]
        public int ComponentNumber { get; set; }

        [Required]
        public string SeriesNr { get; set; }

        [Required]
        public int Componentid { get; set; }
    }
}