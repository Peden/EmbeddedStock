﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AdministrationModule_Group15.Models
{
    public class ComponentViewModel
    {
        [DisplayName("Component name")]
        public string ComponentName { get; set; }

        [DisplayName("Component info")]
        public string ComponentInfo { get; set; }

        [DisplayName("Link to datasheet")]
        public string DataSheetUrl { get; set; }

        [DisplayName("Image of component")]
        public byte[] Image { get; set; }
        public string ImageMimeType { get; set; }

        [DisplayName("Link to manufacture")]
        public string ManufactureLink { get; set; }

        [DisplayName("Category")]
        public int CategoryId { get; set; }


        [DisplayName("Component number")]
        public int ComponentNumberId { get; set; }

        [DisplayName("Component series number")]
        public string SeriesNr { get; set; }
    }
}