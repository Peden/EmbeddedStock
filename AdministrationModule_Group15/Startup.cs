﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdministrationModule_Group15.Startup))]
namespace AdministrationModule_Group15
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
