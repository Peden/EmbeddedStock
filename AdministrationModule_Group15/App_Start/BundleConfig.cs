﻿using System.Web;
using System.Web.Optimization;

namespace AdministrationModule_Group15
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/modernizr-*",
                "~/Scripts/jquery.validate*",
                "~/Scripts/bootstrap.js",
                "~/Scripts/material.min.js",
                "~/Scripts/ripples.min.js",
                "~/Scripts/EmbeddedStock_Scripts/EmbStock.js",
                 "~/Scripts/respond.js"
                        ));

            //"~/Scripts/material-kit.js",
            //                      "~/Styling/material-kit.css",



            bundles.Add(new StyleBundle("~/Styling/css").Include(
                      "~/Styling/bootstrap.min.css",
                      "~/Styling/bootstrap-material-design.css",
                      "~/Styling/ripples.min.css",
                      "~/Styling/EmbStockStyling/site.css",
                      "~/Styling/EmbStockStyling/login_style.css",
                      "~/Styling/EmbStockStyling/side_navigation_style.css",
                      "~/Styling/EmbStockStyling/register_style.css",
                      "~/Styling/EmbStockStyling/HomeStyleSheet.css"));
        }
    }
}
