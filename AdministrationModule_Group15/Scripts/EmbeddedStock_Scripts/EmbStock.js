﻿$(document).ready(() => {

    /*Initialize Bootstrap Material Design*/
    $.material.init();

  $(".addComponentModalBtn").click(() => {

        var componentId = $(".addComponentModalBtn").attr("data-id"); // getting the id for the component which is set in the attribute


        //Generate the correct url that sends request to controller
        var url = "Component/AddOneMoreComponent/" + componentId;



        //Make an AJAX call to get the HTML content for the modal. The View model of this razor view will include the id of the component
        $.get(url, function (data) {
            $('#addComponent_container').html(data);
            $('#addComponentModal').modal('show');
        });
    });

    

    $("#addNewCategoryBtn").click(() => {

        $.get("Nav/AddCategory", (data) => {
            $("#addCategory_container").html(data);
            $("#addCategoryModal").modal("show");
        });
    });


});
