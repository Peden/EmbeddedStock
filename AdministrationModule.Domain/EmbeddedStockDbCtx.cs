﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdministrationModule.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace AdministrationModule.Domain
{
    public class EmbeddedStockDbCtx : IdentityDbContext
    {
        public DbSet<Component> Components { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ComponentNumberAndComment> ComponentNumbersAndComments { get; set; }
        public DbSet<LoanInformation> LoanInformations { get; set; }

        public EmbeddedStockDbCtx()
            : base("EmbeddedStockDbCtx")
        {
            Database.SetInitializer(new CustomInitializer<EmbeddedStockDbCtx>());
        }


        //This method is used for making sure that the ASP.NET Identity framework will make its tables correctly - and know how they should be made
        //Code copied from: http://forums.asp.net/t/1977214.aspx?Mvc+5+Identity+Error+Invalid+object+name+dbo+AspNetUsers+
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityUser>().ToTable("Users").Property(p => p.Id).HasColumnName("UserId"); ;
            modelBuilder.Entity<ApplicationUser>().ToTable("Users").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
            modelBuilder.Entity< IdentityUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
         }




        // Static constructor from the default generated ApplicationDbContext from ASP.NET MVC project with Individual User Accounts Authentication
        public static EmbeddedStockDbCtx Create()
        {
            return new EmbeddedStockDbCtx();
        }





        // This custom db initializer ensures that the db will be dropped and created every time the app start and will automatically seed the db with data
        // Code copied from: http://patrickdesjardins.com/blog/entity-framework-database-initialization
        public class CustomInitializer<T> : DropCreateDatabaseAlways<EmbeddedStockDbCtx>
        {
            public override void InitializeDatabase(EmbeddedStockDbCtx context)
            {
                context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction
                    , string.Format("ALTER DATABASE {0} SET SINGLE_USER WITH ROLLBACK IMMEDIATE", context.Database.Connection.Database));

                base.InitializeDatabase(context);
            }

            protected override void Seed(EmbeddedStockDbCtx context)
            {
                //  This method will be called after migrating to the latest version.
                Category cat1 = new Category { CategoryName = "Transistors" };
                Category cat2 = new Category { CategoryName = "Printboards" };
                Category cat3 = new Category { CategoryName = "ICs" };
                Category cat4 = new Category { CategoryName = "Power Adapters" };

                context.Categories.Add(cat1);
                context.Categories.Add(cat2);
                context.Categories.Add(cat3);
                context.Categories.Add(cat4);

                Component c1 = new Component();
                c1.ComponentName = "Multitransistor Total 23IX";
                c1.ComponentNumbers.Add(new ComponentNumberAndComment { SeriesNr = "5654asd5" });
                c1.ComponentNumbers.Add(new ComponentNumberAndComment { SeriesNr = "5654asdwe" });
                cat1.Components.Add(c1);

                Component c2 = new Component();
                c2.ComponentName = "Special printboard with neon light";
                c2.ComponentNumbers.Add(new ComponentNumberAndComment { SeriesNr = "87543asd" });
                c2.ComponentNumbers.Add(new ComponentNumberAndComment { SeriesNr = "87543wdasd" });
                cat2.Components.Add(c2);

                Component c3 = new Component();
                c3.ComponentName = "Hyper chip Triple Core";
                c3.ComponentNumbers.Add(new ComponentNumberAndComment { SeriesNr = "3242f5qwe8" });
                c3.ComponentNumbers.Add(new ComponentNumberAndComment { SeriesNr = "3255442f5qwe8" });
                cat3.Components.Add(c3);

                Component c4 = new Component();
                c4.ComponentName = "Power adaptor multi extreme 3000";
                c4.ComponentNumbers.Add(new ComponentNumberAndComment { SeriesNr = "6546we5s" });
                c4.ComponentNumbers.Add(new ComponentNumberAndComment { SeriesNr = "ad6546we5s" });
                cat4.Components.Add(c4);

                AddUserAndRoles(context);

                context.SaveChanges();


            }


            //Method taken from: https://azure.microsoft.com/en-us/documentation/articles/web-sites-dotnet-deploy-aspnet-mvc-app-membership-oauth-sql-database/#add-a-database-to-the-application
            void AddUserAndRoles(EmbeddedStockDbCtx context)
            {
                IdentityResult ir;
                var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                ir = rm.Create(new IdentityRole("Admin"));
                ir = rm.Create(new IdentityRole("User"));
                var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = new ApplicationUser()
                {
                    UserName = "admin@admin.com",
                };
                ir =  um.Create(user, "Admin123!");
                if (ir.Succeeded)
                    um.AddToRole(user.Id, "Admin");

            }
        }
    }
}
