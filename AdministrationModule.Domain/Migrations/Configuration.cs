using System.Collections.Generic;
using AdministrationModule.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AdministrationModule.Domain.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AdministrationModule.Domain.EmbeddedStockDbCtx>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "AdministrationModule.Domain.EmbeddedStockDbCtx";

        }



    }



}
