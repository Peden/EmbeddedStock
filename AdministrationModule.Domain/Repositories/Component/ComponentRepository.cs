﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdministrationModule.Domain.Entities;
using AdministrationModule.Domain.Repositories.Component;

namespace AdministrationModule.Domain.Repositories
{
    public class ComponentRepository : AbstractComponentRepo<Entities.Component>
    {
        private EmbeddedStockDbCtx dbcontext = new EmbeddedStockDbCtx();

        public override IEnumerable<Entities.Component> GetAll()
        {
            return dbcontext.Components;
        }
        public override IEnumerable<Entities.Component> GetAllFromCategory(int id)
        {
            return dbcontext.Components.Where(c => c.CategoryId == id).AsNoTracking();
        }



        public override void Delete(int id)
        {
            Entities.Component dbEntry = dbcontext.Components.Find(id);
            if (dbEntry != null)
            {
                dbcontext.Components.Remove(dbEntry);
                dbcontext.SaveChanges();
            }
        }

        public override void DeleteComponentAndComment(int id)
        {
            ComponentNumberAndComment entry = dbcontext.ComponentNumbersAndComments.First(c => c.ComponentId == id);
            if (entry != null)
                dbcontext.ComponentNumbersAndComments.Remove(entry);

            dbcontext.SaveChanges();
        }

        public override void Save(Entities.Component entity)
        {
            if (entity.ComponentId == 0)
                dbcontext.Components.Add(entity);
            else
            {
                Entities.Component component = dbcontext.Components.Find(entity.ComponentId);

                if (component != null)
                {
                    component.Category = entity.Category;
                    component.ComponentName = entity.ComponentName;
                    component.ComponentInfo = entity.ComponentInfo;
                    component.DataSheetUrl = entity.DataSheetUrl;
                    component.ImageMimeType = entity.ImageMimeType;
                    component.Image = entity.Image;
                    component.ManufactureLink = entity.ManufactureLink;
                }

            }

            dbcontext.SaveChanges();
        }

        public override void SaveComponentNumberAndComment(ComponentNumberAndComment componentNrAndComment, int componentId)
        {
            Entities.Component component = dbcontext.Components.Find(componentId);

            if (component != null)
            {
                //check if the component number and comment record already exists - if so it should just be updated!
                ComponentNumberAndComment entry = component.ComponentNumbers.FirstOrDefault(c => c.SeriesNr == componentNrAndComment.SeriesNr);
                if (entry == null)
                    component.ComponentNumbers.Add(componentNrAndComment);
                else
                {
                    entry.AdminComment = componentNrAndComment.AdminComment;
                    entry.UserComment = componentNrAndComment.UserComment;
                }

            }

            dbcontext.SaveChanges();
        }

        public override Entities.Component FindById(int id)
        {
            return dbcontext.Components.Find(id);
        }



    }
}
