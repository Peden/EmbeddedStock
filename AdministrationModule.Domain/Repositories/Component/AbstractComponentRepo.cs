﻿using System.Collections.Generic;
using AdministrationModule.Domain.Entities;

namespace AdministrationModule.Domain.Repositories.Component


{

    /*
     * Abstract class that provides a specified version of IBaseRepository for a Component object
     */
    public abstract class AbstractComponentRepo<T>:IBaseRepository<T> where T:Entities.Component
    {
        public abstract IEnumerable<T> GetAll();

        public abstract IEnumerable<T> GetAllFromCategory(int id);
        public abstract void Delete(int id);
        public abstract void DeleteComponentAndComment(int id);
        public abstract void Save(T entity);
        public abstract void SaveComponentNumberAndComment(ComponentNumberAndComment componentNrAndComment, int componentId);
        public abstract T FindById(int id);
    }
}
