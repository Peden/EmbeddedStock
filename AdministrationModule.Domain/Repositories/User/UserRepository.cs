﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdministrationModule.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AdministrationModule.Domain.Repositories.User
{
    public class UserRepository : AbstractUserRepo<ApplicationUser>
    {
        private EmbeddedStockDbCtx dbcontext = new EmbeddedStockDbCtx();

        public override void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override ApplicationUser FindById(int id)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<ApplicationUser> GetAll()
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<ApplicationUser> GetAllFromCategory(int id)
        {
            throw new NotImplementedException();
        }

        public override void Save(ApplicationUser entity)
        {
            throw new NotImplementedException();
        }

     

        public override async Task<IdentityResult> SaveAsync(ApplicationUser entity, string password)
        {
            var userStore = new UserStore<ApplicationUser>(dbcontext);
                var userManager = new UserManager<ApplicationUser>(userStore);

            var result = await userManager.CreateAsync(entity, password);
            if (result.Succeeded)
                userManager.AddToRole(entity.Id, "Admin"); // A new user is always added as admin right now - to be changed!

            return result;

        }

    

      
    }
}
