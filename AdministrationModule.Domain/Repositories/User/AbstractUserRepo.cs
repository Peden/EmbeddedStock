﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;


namespace AdministrationModule.Domain.Repositories.User
{
    public abstract class AbstractUserRepo<T> : IBaseRepository<T> where T : Entities.ApplicationUser
    {
        public abstract IEnumerable<T> GetAll();

        public abstract IEnumerable<T> GetAllFromCategory(int id);
        public abstract void Delete(int id);
        public abstract void Save(T entity);
        
        public abstract Task<IdentityResult> SaveAsync(T entity, string password);
        public abstract T FindById(int id);
    }
}
