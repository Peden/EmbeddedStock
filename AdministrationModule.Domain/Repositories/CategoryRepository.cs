﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using AdministrationModule.Domain.Entities;

namespace AdministrationModule.Domain.Repositories
{
    public class CategoryRepository : IBaseRepository<Category> 
    {

        private EmbeddedStockDbCtx dbcontext = new EmbeddedStockDbCtx();

        public IEnumerable<Category> GetAll()
        {
            return dbcontext.Categories.AsNoTracking().ToList();
        }



 
        public void Delete(int id)
        {
            Category entry = dbcontext.Categories.Find(id);
            if (entry != null)
                dbcontext.Categories.Remove(entry);
            dbcontext.SaveChanges();
        }

        public void Save(Category entity)
        {
            Category existingCategory = dbcontext.Categories.Find(entity.CategoryId);

            if (existingCategory == null)
                dbcontext.Categories.Add(entity);
            else
            {
                {
                    existingCategory.CategoryName = entity.CategoryName;
                }
            }
            dbcontext.SaveChanges();
        }


        public Category FindById(int id)
        {
            return dbcontext.Categories.Find(id);

        }
    }
}
