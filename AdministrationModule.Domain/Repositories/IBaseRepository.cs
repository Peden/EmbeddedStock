﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministrationModule.Domain.Repositories
{
    public interface IBaseRepository<T> where T:class
    {
        IEnumerable<T> GetAll();
        void Delete(int id);
        void Save(T entity);
        T FindById(int id);
    }
}
