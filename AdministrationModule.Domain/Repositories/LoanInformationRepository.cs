﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdministrationModule.Domain.Entities;

namespace AdministrationModule.Domain.Repositories
{
    public class LoanInformationRepository : IBaseRepository<LoanInformation>
    {
        private EmbeddedStockDbCtx dbcontext = new EmbeddedStockDbCtx();

        public IEnumerable<LoanInformation> GetAll()
        {
            return dbcontext.LoanInformations;
        }
      

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save(LoanInformation entity)
        {
            throw new NotImplementedException();
        }

        public LoanInformation FindById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
