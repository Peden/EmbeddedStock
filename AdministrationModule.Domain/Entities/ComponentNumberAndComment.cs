﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace AdministrationModule.Domain.Entities
{
    public class ComponentNumberAndComment
    {
        [Key]
        public int ComponentNumberId { get; set; } 

        public string SeriesNr { get; set; }

        public string UserComment { get; set; }

        public string AdminComment { get; set; }

        public int ComponentId { get; set; }

        public virtual Component Component { get; set; }

    }
}
