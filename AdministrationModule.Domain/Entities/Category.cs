﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace AdministrationModule.Domain.Entities
{
    public class Category
    {

        public Category()
        {
            Components = new List<Component>();
        }

        public int CategoryId { get; set; }

        [DisplayName("Category name")]
        public String CategoryName { get; set; }

        public virtual ICollection<Component> Components { get; set; }

    }
}
