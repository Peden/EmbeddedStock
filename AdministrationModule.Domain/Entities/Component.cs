﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AdministrationModule.Domain.Entities
{
    public class Component
    {

        public Component()
        {
            ComponentNumbers = new List<ComponentNumberAndComment>();
        }


        [HiddenInput(DisplayValue = false)]
        [Key]
        public int ComponentId { get; set; }

        [DisplayName("Component name")]
        public string ComponentName { get; set; }

        [DisplayName("Component info")]
        public string ComponentInfo { get; set; }

        [DisplayName("Link to datasheet")]
        public string DataSheetUrl { get; set; }
        public byte[] Image { get; set; }
        public string ImageMimeType { get; set; }

        [DisplayName("Link to manufacture")]
        public string ManufactureLink { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual List<ComponentNumberAndComment> ComponentNumbers  { get; set; }

    }
}
