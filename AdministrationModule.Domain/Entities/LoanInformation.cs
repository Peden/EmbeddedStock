﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AdministrationModule.Domain.Entities
{
    public class LoanInformation
    {
        [Key]
        public int LoanInformationId { get; set; }
        public DateTime LoanDate { get; set; }
        public DateTime Returndate { get; set; }
        public string IsEmailSend { get; set; }
        public DateTime ReservationDate { get; set; }
        public string MobileNr { get; set; }
        public string OwnerId { get; set; }
        public string ReservationId { get; set; }


        public virtual Component ComponentId { get; set; }

    }
}
